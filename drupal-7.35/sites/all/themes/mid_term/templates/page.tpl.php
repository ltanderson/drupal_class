<div class="site-container no-bg no-border">
    <!-- site title -->
    <div class="site-title-container">
        <div class="site-title">
            <p><?php print $site_name ?></p>
        </div>
    </div>
</div>

<div class="site-container">
    
    <div class="content-container container">
        <div class="menu-container">
            <?php
                print theme('links',array('links'=>$main_menu));
            ?>
        </div>
        
        <div class="content clearfix">
            
            <!-- messages will go here -->
            <?php if ($messages): ?>
                <div id="messages">
                    <?php print $messages; ?>
                </div>
            <?php endif; ?>
            
            <!-- tabs will go here -->
            <?php if ($tabs): ?>
                <div class="tab-container container">
                    <?php print render($tabs); ?>
                </div>
            <?php endif; ?>
            
            <!-- left column region -->
            <?php if($page['left_column']): ?>
                <div class="left-column column region one-fourth left">
                    
                    <div class="inner">
                        <h3><i class="fa fa-list"></i> Recent posts</h3>
                        <?php print render($page['left_column']); ?>
                    </div>
                </div>
            <?php endif; ?>
            
            <div class="main-content three-fourths left">
                
                <!-- main content -->
                <div class="title">
                    <h1><?php print $title; ?></h1>
                </div>
                
                <div class="content">
                    
                    <?php
                        print render($page['content']);
                    ?>
                </div>
            </div>
        </div>
    </div>
    
    <!-- footer region -->
    <div class="footer-container container">
        <div class="footer-content inner-container">
            <?php
                print render($page['footer']);
            ?>
        </div>
    </div>
    
</div>