<div class="site-container">
     
    <div class="gradient-overlay">
    </div>
    
    <!-- header -->
    <div class="header-wrapper">
    </div>
    
    <!-- site title -->
    <div class="site-title-container">
        <div class="site-title">
            <p>Photography Blog</p>
        </div>
    </div>
    
    <!-- content -->
    <div class="content-container container">
        <div class="menu-container inner-container">
            
            <?php 
                print theme('links',array('links'=>$main_menu));
            ?>
            
        </div>
        
        <!-- tabs will go here -->
        <div class="tab-container container">
        </div>
          
        <div class="content inner-container clearfix">
            <div class="main-content <?php print $variables['new_theme']['main-content-width']; ?> left">
                <div class="content">
                    
                    <!-- messages will go here -->
                    <div id="messages">
                    </div>
                    
                    <p></p>
                    <div>
                        <?php
                            print render($page['content']);
                        ?>
                    </div>
                </div>
            </div>
            <?php if($page['aside']): ?>
            <div class="right-column column region one-fourth left">
                <?php
                    print render($page['aside']);
                ?>
                
                <!-- <div class="region-inner">

                    <ul class="photo-categories">
                        <li>
                            <div class="block">
                                <div class="inner">
                                    <img src="img/boz_skate.jpg" />
                                    <div class="category">
                                        <p class="label">
                                            <a href="#" class="more">Friends</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="block">
                                <div class="inner">
                                    <img src="img/ferrari.jpg" />
                                    <div class="category">
                                        <p class="label">
                                            <a href="#" class="more">Historic Races</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="block">
                                <div class="inner">
                                    <img src="img/miner_show.jpg" />
                                    <div class="category">
                                        <p class="label">
                                            <a href="#" class="more">Music</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div> -->
            </div>
            <?php endif; ?>
        </div>
    </div>
    
    <!-- footer -->
    <div class="footer-container container">
        <div class="footer-content inner-container">
            <?php print render($page['footer']); ?>
        </div>
    </div>
    
</div>