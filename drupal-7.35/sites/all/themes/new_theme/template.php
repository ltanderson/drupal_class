<?php

	function new_theme_preprocess_page(&$variables){
		$page = $variables["page"];

		$variables["new_theme"]["main-content-width"] = "three-fourths";

		if(empty($page["aside"])){
			$variables["new_theme"]["main-content-width"] = "full-width";
		}
		
	}

 ?>
 